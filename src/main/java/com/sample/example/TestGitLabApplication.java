package com.sample.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestGitLabApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestGitLabApplication.class, args);
	}
}
